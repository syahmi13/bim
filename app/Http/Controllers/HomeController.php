<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function abjad()
    {
        $gambars = Gambar::where('type', 'abjad')->get();
        return view('abjad', compact('gambars'));
    }

     public function nombor()
    {
        $gambars = Gambar::where('type', 'nombor')->get();
        return view('nombor', compact('gambars'));
    }

     public function carian(Request $request)
    {


         $gambars = Gambar::query();
        //dd($gambars);

        if($request->isMethod('post')){
            $serch = $request->input('serch');

            $query = explode(' ',  strtolower($serch));

            if( $serch)
            $gambars->whereIn(\DB::raw('lower(name)'), $query);
            
        }

        $gambars = $gambars->get();
        return view('carian', compact('gambars', 'serch'));
    }

    public function list()
    {
         $gambars = Gambar::all();
        //dd($gambars);
        return view('list', compact('gambars'));
    }

    public function add(Request $request)
    {
        if ($request->isMethod('post')) {
            $name = $request->input('name');
            $type = $request->input('type');
            $picture = $request->file('picture');

            $filename=$picture->getClientOriginalName();
            $picture->move(public_path().'/files/' , $filename);

            $gambar = new Gambar;
            $gambar->name = $name;
            $gambar->type = $type;
            $gambar->picture_path = '/files/' . $filename;
            $gambar->save();

        }
        return view('add');
    }




}
